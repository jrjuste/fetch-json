import { Component } from '@angular/core';
import { CrudHttpService } from './crud-http.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  serverList: any = [];

  checkedBoxes = document.querySelectorAll('input[name=mycheckboxes]:checked');

  constructor(private crudHttpService: CrudHttpService) {

    console.log(this.checkedBoxes);
  }

  ngOnInit(): void {

    this.listServers();

  }

  //refresh
  listServers() {
    this.crudHttpService.list().subscribe(
      (response) => {
        this.serverList = response;
      },
      (error) => {}
    );
  }

  //clear checkbox
  public clearCheck() {
    document.querySelectorAll('.checkbox').forEach(_checkbox=>{
      (<HTMLInputElement>_checkbox).checked = false;
    });
  }

  //check all boxes
  public checkAll() {
    document.querySelectorAll('.checkbox').forEach(_checkbox=>{
      (<HTMLInputElement>_checkbox).checked = true;
    });
  }



}
